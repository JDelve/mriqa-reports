#!/usr/bin/env python
"""
Clone Templates to generate a new directory for report. Better with individual templates per scanner for now.
"""

SCANNERS = {
    'MRC27202':        'BHIAvantoFit',
    'MRC37294':        'BRISymphonyTim',
    'MRC25843':        'BRIAvantoFit',
    'AWP42400':        'BCHAera',
    'AWP45622':        'BCHSkyra',
    'CRIC45064':       'CRICSkyra',
    'WSM-MR1':        'WGHAvantoFit',
    'AWP196245':       'SBCHLumina',
    'AWP182902':       'SiemensXA',
    'PHILIPS-VL9UFAM': 'NBTL1Ingenia3',
    'MRl1R1':          'NBTL1Discovery',
    'PHILIPS-COLQCK3': 'NBTL2Ingenia15',
    'HOST-9THVNMQAOJ': 'NBTL2Ingenia15', # changed Jan 2020 ?
    'PHILIPS-3FDMLA4': 'NBTL2Achieva3',
    'PHILIPS-4ED482A': 'CMHIntera'
}

if __name__ == '__main__':
    """
    Copy templates into new report directory.
    """
    import sys
    from glob import glob
    from os.path import join, abspath, dirname
    from os import makedirs
    from shutil import copy
    from argparse import ArgumentParser
    import yaml

    parser = ArgumentParser(
        description='Clone templates for a new report directory'
    )

    parser.add_argument('--scanner', action='store', type=str, choices=list(SCANNERS), required=False, help='Scanner Node Name')

    parser.add_argument('reportdir', type=str, help='new report directory')
    args = parser.parse_args()

    try:
        makedirs(args.reportdir, exist_ok=True)
    except OSError as e:
        print(f'{sys.argv[0]}: Unable to create report directory "{args.reportdir}"', file=sys.stderr)
        sys.exit(1)

    if args.scanner is None:
        yaml_file = abspath(join(dirname(args.reportdir), 'node_info.yml'))
        with open(yaml_file) as fp:
            node_info = yaml.safe_load(fp)
        scanner_type = SCANNERS[node_info['station_name']]
    else:
        scanner_type = SCANNERS[args.scanner]

    templates_dir = abspath(join(args.reportdir, '..', '..', '..', 'Templates'))
    template_file = f'AQA{scanner_type}.ipynb'
    copy(join(templates_dir, template_file), args.reportdir)
    copy(join(templates_dir, 'Makefile'), args.reportdir)
    copy(join(templates_dir, 'study.yml'), args.reportdir)

    sys.exit(0)
