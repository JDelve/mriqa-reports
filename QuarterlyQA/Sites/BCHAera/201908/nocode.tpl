{#
  nocode.tpl:
    A nbconvert jinja2 template for togglable hidden code cells.
    Based on https://gist.github.com/gabraganca/7097067.
    R.HartleyDavies@bristol.ac.uk 2015-04-08
#}
{% extends 'full.tpl' %}
{% block input_group %}
<!-- wrap in a div so we can toggle visibility -->
<div class="input_hidden">
{{ super() }}
</div>
{% endblock input_group %}
{%- block header -%}
{{ super() }}
<style type="text/css">
/* code cells hidden by default */
.input_hidden {
  display: none;
}
/* all prompts hidden */
div.prompt {
  display: none;
}
</style>
<script>
// toggle visibility of code cells
// nb jquery 2.0.3 already in full.tpl
$(document).ready(function(){
  $(".output_wrapper").click(function(){
    $(this).prev('.input_hidden').slideToggle();
  });
})
</script>
{%- endblock header -%}
