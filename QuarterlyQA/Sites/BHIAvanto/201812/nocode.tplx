% nb the tags with brackets are actually jinja2 {% %} changed to avoid clashes with latex tags
% Base template
((* extends 'article.tplx' *))

% Reduce point size and stop figures floating
((* block docclass *))
\documentclass[9pt]{article}
\usepackage{float}
\floatplacement{figure}{H}
((* endblock docclass *))


% Reduce Margins
((* block margins *))
\geometry{verbose,tmargin=0.8in,bmargin=0.8in,lmargin=0.8in,rmargin=0.8in}
((* endblock margins *))

% Disable input cells
((* block input_group *))
((* endblock input_group *))

% Turn off numbering for sections
((* block commands *))
\setcounter{secnumdepth}{0}
((( super() )))
((* endblock commands *))

% No Author field
((* block author *))
((* endblock author *))

% Modified title (from metadata)
((* block title *))
((*- if nb.metadata["latex_metadata"]: -*))
((*- if nb.metadata["latex_metadata"]["title"]: -*))
    \title{((( nb.metadata["latex_metadata"]["title"] | ascii_only )))}
((*- endif *))
((*- else -*))
    \title{((( resources.metadata.name | ascii_only | escape_latex )))}
((*- endif *))
((* endblock title *))

% Empty date
((* block date *))
\date{}
((* endblock date *))

% Default mechanism for rendering figures
((*- block data_png -*))((( draw_figure_sized(output.metadata.filenames['image/png'], cell['metadata'].get('nbconvert', {}).get('figsize', 0.9)) )))((*- endblock -*))
((*- block data_jpg -*))((( draw_figure_sized(output.metadata.filenames['image/jpeg'], cell['metadata'].get('nbconvert', {}).get('figsize', 0.9)) )))((*- endblock -*))
((*- block data_svg -*))((( draw_figure_sized(output.metadata.filenames['image/svg+xml'], cell['metadata'].get('nbconvert', {}).get('figsize', 0.9)) )))((*- endblock -*))
((*- block data_pdf -*))((( draw_figure_sized(output.metadata.filenames['application/pdf'], cell['metadata'].get('nbconvert', {}).get('figsize', 0.9)) )))((*- endblock -*))

% Draw a figure using the graphicx package.
((* macro draw_figure_sized(filename, size=0.9) -*))
((* set filename = filename | posix_path *))
((*- block figure scoped -*))
    \begin{center}
    \adjustimage{max size={((( size )))\linewidth}{((( size )))\paperheight}}{((( filename )))}
    \end{center}
%    { \hspace*{\fill} \\}
((*- endblock figure -*))
((*- endmacro *))

((* set cell_style = 'style_bw_python.tplx' *))

