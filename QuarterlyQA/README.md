# Quarterly MRI QA

The QA is performed with a simple bottle phantom in the most used head coil. The main acqusitions are a simple spin-echo, preserving individual coil element images and where possible the same acquisition repeated with the RF transmitter off (by setting the transmitter voltage to zero on Siemens systems).

Quarterly QA patients should be registered under a name of the form:
```
QQAYYYYMMDD<CODE>
```
where `YYYYMMDD` is the scan date in [ISO_8601](https://en.wikipedia.org/wiki/ISO_8601) basic form and `<CODE>` is a unique abbreviation designating the scanner.

|     Scanner    |  Station Name |Code
|:---------------|:--------------|:---
|BRI Symphony    |MRC37293       |BRIST
|BRI AvantoFit   |MRC25845       |BRIAF
|BHI Avanto      |MRC27202       |BHIAV
|BCH L3 Aera     |AWP42400       |BCHAE
|BCH L4 Skyra    |AWP45622       |BCHSK
|CRIC Skyra      |CRIC45064      |CRICS
|Weston AvantoFit|MRC37294       |WGHAF
|SMD L1 GE MR450 |MRl1R1         |SM1GE
|SMD L1 Ingenia  |PHILIPS-VL9UFAM|SM1IG
|SMD L2 Ingenia  |PHILIPS-COLQCK3|SM2IG
|SMD L2 Achieva  |PHILIPS-3FDMLA4|SM2AC
|Cossham Intera  |PHILIPS-4ED482A|COSIT

NB: This will change when automatic rewriting of Patient IDs is implemented on `BIRCHQA` when a single Patient ID per scanner will be sufficient. 

## Siemens Scanners
### Acquisition
This is usually performed on the Siemens *long bottle* phantom in the most used head coil - either the standard Head and Neck 25 channel coil or the 32 channel head coil. These are used for the Siemens coil QA and it can be easily and reproducibly positioned using the dedicated Siemens foam pad. The phantom is slightly different on VB systems (Avanto and SymphonyFit).

Where this phantom is not available (eg on some Avanto generation systems) then either the large "long bottle" or the slightly wider "short bottle" and their corresponding holders should be used.

The acquisition protocol consists of the following scans:
 - SE (SNR, Uniformity, XY gradient scale)
   - Prescan Normalisation, but keep original images, two acquisitions
   - Keep single element images
   - RMS coil combination mode (use "triple" mode on VB systems)
 - SE (Noise Only)
   - No Prescan Normalisation, two acquisitions
   - Keep single element images
   - RMS coil combination mode (use "triple" mode on VB systems)
   - Transmitter reference voltage set to *zero* (set in manual adjustments on VB systems)

and optionally:
 - EPI, 60+ acquisitions for ghosting and long term stability, eddy currents and shim (distortion)

Note that for VE systems the transmitter voltage can be set directly in the System tab of the UI. On Avanto generation systems however, this can only be done from within the manual adjustments tool.

The bandwidth used for the SE acquisitions should be approximately scaled with the field strength so 130Hz at 1.5T and 260Hz at 3.0T and a fixed T<sub>R</sub>/T<sub>E</sub> of 500/30 ms

The phantom should be positioned on the Siemens foam holder, the laser reference set to middle of the coil as identified by marks on the coil casing and the scan performed axially with no x or z offset. An AP offset should be manually applied so that the centre is the image is approximately the centre of the phantom. It is convenent if the protocol is saved with the appropriate AP offset. A localizer should be acquired to confirm the positioning.

For the VE system's 25 channel head and neck *all* elements including the neck elements should be switched on, even though the system by default turns the neck elements off. However where the neck elements are in a separate and detachable part as is the case on the VB systems then the neck part should be left off and all remaining elements selected.
 
Following the localizer, the acquisitions should have the following labels in the protocol to make them easier to identify in the analysis:
 - `QQA_SE_SIGNAL`
 - `QQA_SE_NOISE`
 - `QQA_EPI_STABILITY`

Images should be saved to the DICOM destination `BIRCHQA` either directly from the scanner where possible or via other DICOM servers such as `PHYSICSQA` otherwise.

### Analysis
This is best performed on the server `birchhub`. This can be accessed by in the following ways:
 - ssh for a terminal session (eg using [putty](https://en.wikipedia.org/wiki/PuTTY]))
 - via [Jupyterhub](http://192.168.23.223/jupyterhub)
 - via a nomachine remote desktop

Within a clone of the bitbucket repository [mriqa-reports](https://bitbucket.org/rtrhd/mriqa-reports) in the subdirectory for the scanner under `QuarterlyQA/Sites` create a new directory with a name corresponding the the date of the scan in [ISO_8601](https://en.wikipedia.org/wiki/ISO_8601) basic format (eg 19991231) and copy in the following files from the `QuarterlyQA/Templates` directory:

 - an unpopulated template notebook,
 - `study.yml`
 - `Makefile`
 
Use the `QQASiemensVE.ipynb` and the `QQASiemensVB.ipynb` notebook templates as appropriate. This may be also accomplished in a single step with the script `newreport`.

```
$ QuarterlyQA/scripts/newreport QuarterlyQA/Sites/BCHSkyra/20191231

```


In the new directory, edit the `study.yml` file to reflect the details of the study including the patient id and the series numbers of the relevant scans. The series numbers can change if, for instance, multiple localizers are acquired. The details can be obtained by looking up the scans on the Orthanc DICOM server on [birchqa](http://192.168.23.223/) Then run the notebook from within [Jupyterlab](https://jupyterlab.readthedocs.io/en/stable/) in order to perform the analysis and save the resulting notebook with the results.

To publish the results as a `pdf` file type `make` within the directory or run the script `publishqqa`. The `pdf` will be named based on the date of the QA scans and the `Station Name` of the scanner and will be left in the working directory unless otherwise specified.


## Philips Scanners


## GE Scanners
