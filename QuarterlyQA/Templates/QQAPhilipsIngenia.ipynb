{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Quarterly QA on Philips Scanners"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is performed on the Philips \"1.5L bottle\" phantom in the most used head coil.\n",
    "\n",
    "The bandwidth used should be approximately scaled with the field strength so 130Hz at 1.5T and 260Hz at 3.0T."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The quarterly QA scans consist of the following scans:\n",
    " - SE, 2 acquisitions, with/without PSN, multiple elements, RMS coil combine -> SNR, Unif, XY gradient scale\n",
    " - SE, 2 acquisitions, without PSN, multiple elements, RMS coil combine, TX switched off by setting reference voltages to zero\n",
    " - EPI, 60+ acquisitions, ghosting and long term stability, eddy currents and shim (distortion)\n",
    "\n",
    "The acquistions should have the following labels in the protocol to make them easier to identify:\n",
    " - `QQA_SE_SIGNAL`\n",
    " - `QQA_SE_NOISE`\n",
    " - `QQA_EPI_STABILITY`\n",
    " \n",
    "Although we may well have multiple series for each label we should be able to deduce which is which based on the image type, coil names and coil element masks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import division, print_function\n",
    "\n",
    "# Patient details file yaml/json\n",
    "import yaml\n",
    "\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from mriqa.phantoms import PHILIPS1L5BOTTLE\n",
    "\n",
    "from mriqa.dcmio import fetch_series, coil_elements\n",
    "from mriqa.tools import show_montage\n",
    "\n",
    "from mriqa.reports import (\n",
    "    ghosting_report, uniformity_report, noise_correlation_report,\n",
    "    circularity_report, snr_report_multi, snr_report\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Details of series to analyse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('study.yml') as f:\n",
    "    study_info = yaml.safe_load(f)\n",
    "patid = study_info['PatID']\n",
    "studyid = study_info['StudyID']\n",
    "series = study_info['Series']\n",
    "\n",
    "phantom = PHILIPS1L5BOTTLE\n",
    "\n",
    "display(\n",
    "    pd.DataFrame(\n",
    "        {'Details':[patid, studyid] + list(series.values())}, \n",
    "        index=['Patient', 'Study'] + list(series.keys())\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Individual coil element images"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Assume just one element\n",
    "dobjs = fetch_series(patid, studyid, sernos=series['Signal'], imagesonly=True)\n",
    "show_montage(dobjs, None, op='mean', cmap='viridis', title='Per-channel Signal Images');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Signal to Noise Ratio"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Signal to noise ratio using uncombined signal images, which we combine here as sum of squares."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SNR (uncombined channels)\n",
    "dobjs = fetch_series(patid, studyid, sernos=series['Signal'], imagesonly=True)\n",
    "snr_report_multi(dobjs, None, phantom=phantom)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SNR (combined channels)\n",
    "dobjs = fetch_series(patid, studyid, sernos=series['Signal_Combined'], imagesonly=True)\n",
    "snr_report(dobjs, None, phantom=phantom)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Geometry\n",
    "For geometrical measures we want to look at the average of the coil-combined images with intensity correction applied. This will give us the highest SNR and the most uniform image, which should make the segmentation more robust wthout affecting the image geometry."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Phantom Circularity (Scale and Distortion)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dobjs = sorted(\n",
    "    fetch_series(patid, studyid, sernos=series['Geometry'], imagesonly=True),\n",
    "    key=lambda x: (int(x.SeriesNumber), int(x.AcquisitionNumber))\n",
    ")\n",
    "circularity_report(dobjs[0], phantom=phantom, excluded_sector=38).T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ghosting\n",
    "We can use the same routines for ghosting as for the small GE sphere. As the phantom is cylindrical, of course, only the axial plane will be considered.\n",
    "\n",
    "Note that the actual values obtained for the ghosting ratio will vary somewhat according to exactly where the ghosting regions of interest are defined. In particular, any ringing-like behaviour will only be detected with a ROI close to the phantom. Nyquist ghosting on the other hand is best detected with an ROI that comprises the phantom shifted by N/2 in the *phase encoding* direction.\n",
    "\n",
    "As the test compares background signal in the phase encoding and the readout direction we are only sensitive to these separately (generally in the phase encoding direction). Anything that is manifest on both axes will cancel out in the subtraction. Possibly a better point of comparison would be the image corners."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# BOTTLE: Ghosting\n",
    "dobjs = fetch_series(patid, studyid, sernos=series['Signal_Combined'], imagesonly=True)\n",
    "\n",
    "results = ghosting_report([dobjs[0]], phantom=phantom)\n",
    "results[['Protocol', 'Orientation', 'Sequence', 'GhostRatio']].T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Uniformity\n",
    "These are the same routines for uniformity as for the oil sphere used at acceptance but the phantom is cylindrical so only the axial plane is considered. Note that, at 3T there will be a significant contribution from the $B_1^+$ non-uniformity as well as from the receive coil sensitivities. Only some of this may be compensated for by the prescan normalize option; the scans used to calibrate this may have different sensitivity to flip angle variations. On the other hand this is quite a small phantom so the $B_1^+$ effects may not be very pronounced over such a limited volume."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_dobjs = fetch_series(patid, studyid, sernos=series['Signal_Combined'], imagesonly=True)\n",
    "psn_dobjs = fetch_series(patid, studyid, sernos=series['Geometry'], imagesonly=True)\n",
    "\n",
    "df = uniformity_report(raw_dobjs=raw_dobjs[:1], psn_dobjs=psn_dobjs[:1], phantom=phantom)\n",
    "df[['XUniformityRaw', 'YUniformityRaw', 'NEMAUniformityRaw']].T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df[['XUniformityNorm', 'YUniformityNorm', 'NEMAUniformityNorm']].T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Frequency Reference and Transmitter Voltage\n",
    "These are the reference values reported in the DICOM headers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dobj = raw_dobjs[0]\n",
    "pd.DataFrame({\n",
    "    'ImagingFrequency': [float(dobj.ImagingFrequency)],\n",
    "    'TransmitterCalibration': [float(csa_tags['TransmitterCalibration'])],\n",
    "    'TalesReferencePower': [float(csa_tags['TalesReferencePower'])],\n",
    "    'GradientSensitivityX': [\n",
    "        float(phoenix_tags['sGRADSPEC.flSensitivityX']) * 1e6,\n",
    "    ],\n",
    "    'GradientSensitivityY': [\n",
    "        float(phoenix_tags['sGRADSPEC.flSensitivityY']) * 1e6,\n",
    "    ],\n",
    "    'GradientSensitivityZ': [\n",
    "        float(phoenix_tags['sGRADSPEC.flSensitivityZ']) * 1e6\n",
    "    ]\n",
    "}, index=['Value']).T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
